

// gọi dối tượng DanhSachSinhVien là đối tượng chứa các chức năng
var DanhSachSinhVien = new DanhSachSinhVien()

// load get storage
GetStorage();


// gọi đối tượng kiểm tra giá trị đầu vào
var validate = new Validation()

// function DomID
function DomID(id) {
    var element = document.getElementById(id);
    return element;
}


// function với chức năng là ThemSinhVien 
function ThemSinhVien() {
    // Lấy dữ liệu của người dùng nhập vào
    var maSv  = DomID('txtMaSV').value
    var tenSv  = DomID('txtTenSV').value
    var email  = DomID('txtEmail').value
    var password  = DomID('txtPass').value
    var date  = DomID('txtNgaySinh').value
    var course  = DomID('khSV').value
    var diemToan  = DomID('txtDiemToan').value
    var diemLy  = DomID('txtDiemLy').value
    var diemHoa  = DomID('txtDiemHoa').value
    
    
    // Kiem tra validate
    let test = 0
    if(KiemTraRong('txtMaSV',maSv) == true) {
        test++;
    }
    if(KiemTraRong('txtTenSV',tenSv) == true) {
        test++;
    }
    if (validate.KiemTraEmail(email) == true) {
        DomID('txtEmail').style.borderColor = 'green'
    } else {
        DomID('txtEmail').style.borderColor = 'red'
        test++
    }
    if(KiemTraRong('txtPass',password) == true) {
        test++;
    }
    if(KiemTraRong('txtNgaySinh',date) == true) {
        test++;
    }
    if(validate.KiemTraKhoaHoc(course)) {
        DomID('khSV').style.borderColor = 'green'
    } else{
        DomID('khSV').style.borderColor = 'red'
        test++
    }
  
    if (validate.KiemTraDiem(diemToan)) {
        DomID('txtDiemToan').style.borderColor = 'green'
    } else{
        DomID('txtDiemToan').style.borderColor = 'red'
        test++
    }
    if (validate.KiemTraDiem(diemLy)) {
        DomID('txtDiemLy').style.borderColor = 'green'
    } else{
        DomID('txtDiemLy').style.borderColor = 'red'
        test++
    }
    if (validate.KiemTraDiem(diemHoa)) {
        DomID('txtDiemHoa').style.borderColor = 'green'
    } else{
        DomID('txtDiemHoa').style.borderColor = 'red'
        test++
    }
    if (test != 0) {
        return false;
    }

    // valiable gọi đến Đói tượng SinhVien và truyền value vào
    let sinhVien = new SinhVien(maSv, tenSv, email, password, date, course, diemToan, diemLy, diemHoa);
    
    // truyền Sinh Viên vào Ham ThemSinhVien
    DanhSachSinhVien.ThemSinhVien(sinhVien);

    // GỌi hàm in ra table
    CapNhapDanhSachSinhVien(DanhSachSinhVien.DSSV)

    // Lưu vào JSOn 
    //1. ép thành string bằng JSON.stringify because aplication only save string 
    //2. after data compression successful then use localStorage.setItem with param 1 is name and 2 is data
    let jsonDanhSachSinhVien = JSON.stringify(DanhSachSinhVien.DSSV)
    localStorage.setItem('DanhSachSV',jsonDanhSachSinhVien)
}



// function validate value user Strim
function KiemTraRong(Id, value) {
    if(validate.KiemTraRong(value)) {
        DomID(Id).style.borderColor = 'red'
        return true;
    }else {
        DomID(Id).style.borderColor = 'green'
        return;
    }
}


// Function in SinhVien ra table
function CapNhapDanhSachSinhVien (DanhSachSinhVien) {
    let contentHTML = '';

    // duyêt mảng = forEach
    DanhSachSinhVien.forEach(function (sinhVien) {
        return contentHTML += `<tr>
        <td>${sinhVien.MaSv}</td>
        <td>${sinhVien.HoTen}</td>
        <td>${sinhVien.Email}</td>
        <td>${sinhVien.Date}</td>
        <td>${sinhVien.Course}</td>
        <td>
        <input class="checkMaSV" type="checkbox" value="${sinhVien.MaSv}"/>  
        </td>
    </tr>`
    }) 
    // <td>${sinhVien.DTB()}</td>

    document.getElementById('tbodySinhVien').innerHTML = contentHTML
}
   
// function Get Storage (hàm lấy dữ liệu trong kho aplication)
function GetStorage() {
    // how to getStorage in JSOn
    //1. Creat valiable = localStorage.getIten with param is name of data save in JSOn
    //2. after creat array = JSON.parse() width param is valiable = localStorage
    //3. final call to array main = array creat
    let jsonDanhSachSinhVien = localStorage.getItem('DanhSachSV')
    let mangDSSV = JSON.parse(jsonDanhSachSinhVien)
    DanhSachSinhVien.DSSV = mangDSSV;
    CapNhapDanhSachSinhVien( DanhSachSinhVien.DSSV)
}


//Xoa Sinh Vien
function XoaSinhVien() {
    let listMaSVXoa = document.querySelectorAll('.checkMaSV');
    let arrSvXoa = [];
    for (var i = 0; i < listMaSVXoa.length; i++) {
        if(listMaSVXoa[i].checked) {
            arrSvXoa.push(listMaSVXoa[i].value)
        }
    } 
    
    DanhSachSinhVien.XoaSinhVien(arrSvXoa)
    let saveJson = JSON.stringify(DanhSachSinhVien.DSSV) 
    localStorage.setItem('DanhSachSV',saveJson)
    CapNhapDanhSachSinhVien(DanhSachSinhVien.DSSV)
}

function TimKiem() {
     let tuKhoa = DomID('txtSearch').value;
     let arrSvSearch = []
     DanhSachSinhVien.DSSV.forEach(function (sinhVien) {
        if(sinhVien.HoTen.toLowerCase().trim().search(tuKhoa.toLowerCase().trim()) != -1) {
            arrSvSearch.push(sinhVien)
        }
     })  
    CapNhapDanhSachSinhVien(arrSvSearch)
}