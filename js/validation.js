
// function Validation chức năng kiểm tra giá trị đầu vào.
function Validation() {
    this.KiemTraRong = function (value) {
        if(value.trim() === '') {
            return true;
        } 
        return false;
    }
    this.KiemTraEmail = function(value) {
        var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
        return re.test(value.toLowerCase())
    }
    this.KiemTraKhoaHoc = function (value) {
        if(value == 0) {
            return false
        } else {
            return true
        }
    }
    this.KiemTraDiem = function (value) {
        if(!value.match(/^\d+/) || value > 10 || value < 0 || value.trim() === '') {
            return false;
        } else {
            return true;
        } 
    }
} 